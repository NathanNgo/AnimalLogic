# Animal Logic Technical
Animal Logic technical task.

## Gitlab Continuous Integration
https://gitlab.com/NathanNgo/AnimalLogic

## Installation
To install necessary dependencies: `pip3 install -r requirements.txt`.

## Usage
* To run the main program: `python3 main`.
* To run the unit tests: `python3 -m pytest --testdox`. Ensure you are in the projects root directory.

## Additional Information
* All supported formats are in the `formats/` directory.
* Additional formats can be added by implementing a class with `read`, `write`, and `display` methods.
* To add a new format to the main module, add a new key-value pair to the `formats` dictionary.
