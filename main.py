from formats.json_format import JsonFormat
from formats.yaml_format import YamlFormat


formats = {
    "json": JsonFormat,
    "yaml": YamlFormat
}
available_formats = ', '.join(formats.keys())


##
# Main method. Starts the event loop and command processing.
#
def main():
    data = None

    while True:
        cmd = input("> Select an action (read, write, display, exit): ").strip().lower()

        if cmd == "read":
            data = read()
        elif cmd == "write":
            write(data)
        elif cmd == "display":
            display(data)
        elif cmd == "exit":
            break
        else:
            print("Invalid command")

    print("Exiting")


##
# Performs the read action for the selected file and format.
#
def read():
    data = None
    path = input("> Enter file path: ")
    format_type = input(f"> Select file type ({available_formats}): ")

    try:
        data = formats[format_type].read(path)
        print("File successfully read")
    except (FileNotFoundError, IsADirectoryError):
        print("Invalid path")
    except KeyError:
        print("Invalid format")
    except RuntimeError:
        print("Failed to read file")

    return data


##
# Performs the write action for the selected file and format.
#
def write(data):
    if data is not None:
        path = input("> Enter file path: ")
        format_type = input(f"> Select file type ({available_formats}): ")

        try:
            formats[format_type].write(path, data)
            print("File successfully written")
        except (FileNotFoundError, IsADirectoryError):
            print("Invalid path")
        except KeyError:
            print("Invalid format")
        except RuntimeError:
            print("Failed to write data")
    else:
        print("There is no data to write")


##
# Displays the data onto the terminal in the selected format.
#
def display(data):
    if data is not None:
        format_type = input(f"> Select display type ({available_formats}): ")

        try:
            formats[format_type].display(data)
        except KeyError:
            print("Invalid format")
        except RuntimeError:
            print("Failed to display data")
    else:
        print("There is no data to display")


if __name__ == "__main__":
    main()
