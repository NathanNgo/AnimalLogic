import yaml


class YamlFormat:

    @staticmethod
    def read(file):
        with open(file, 'r') as f:
            try:
                return yaml.load(f, Loader=yaml.FullLoader)
            except Exception:
                raise RuntimeError

    @staticmethod
    def write(file, data):
        with open(file, 'w') as f:
            yaml.dump(data, f)

    @staticmethod
    def display(data):
        print(yaml.dump(data))
