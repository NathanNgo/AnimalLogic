import json


class JsonFormat:

    @staticmethod
    def read(file):
        with open(file, 'r') as f:
            try:
                return json.load(f)
            except Exception:
                raise RuntimeError

    @staticmethod
    def write(file, data):
        with open(file, 'w') as f:
            json.dump(data, f, sort_keys=True)

    @staticmethod
    def display(data):
        print(json.dumps(data, indent=4, sort_keys=True))
