import pytest
import main
import formats.json_format


@pytest.mark.it("Event loop correctly enters read function on read action")
def test_main_enters_read(mocker):
    mocker.patch("builtins.input", side_effect=["read", "exit"])
    mocker.patch("main.read")

    main.main()
    (args, kwargs) = main.read.call_args

    assert args == ()


@pytest.mark.it("Event loop correctly enters write function on write action")
def test_main_enters_write(mocker):
    mocker.patch("builtins.input", side_effect=["write", "exit"])
    mocker.patch("main.write")

    main.main()
    (args, kwargs) = main.write.call_args

    assert args[0] is None


@pytest.mark.it("Event loop correctly enters display function on display action")
def test_main_enters_display(mocker):
    mocker.patch("builtins.input", side_effect=["display", "exit"])
    mocker.patch("main.display")

    main.main()
    (args, kwargs) = main.display.call_args

    assert args[0] is None


@pytest.mark.it("Event loop correctly exits on exit command")
def test_main_exits(mocker):
    mocker.patch("builtins.input", return_value="exit")
    mocker.patch("builtins.print")

    main.main()

    print.assert_called()


@pytest.mark.it("Event loop correctly detects an invalid command")
def test_main_invalid_command(mocker):
    mocker.patch("builtins.input", side_effect=["not valid", "exit"])
    mocker.patch("builtins.print")

    main.main()
    (args) = print.call_args_list

    assert args[0][0][0] == "Invalid command"


@pytest.mark.it("Read function correctly reads path")
def test_main_read_valid(mocker):
    mocker.patch("builtins.input", side_effect=["./test", "json"])
    mocker.patch("formats.json_format.JsonFormat.read", return_value="Some data")

    data = main.read()
    assert data == "Some data"


@pytest.mark.it("Read function detects invalid path")
def test_main_read_invalid_path(mocker):
    mocker.patch("builtins.input", side_effect=["", "json"])
    mocker.patch("formats.json_format.JsonFormat.read", side_effect=FileNotFoundError)
    mocker.patch("builtins.print")

    main.read()
    (args, kwargs) = print.call_args

    assert args[0] == "Invalid path"


@pytest.mark.it("Read function detects invalid path with directory")
def test_main_read_invalid_path_directory(mocker):
    mocker.patch("builtins.input", side_effect=["./", "json"])
    mocker.patch("formats.json_format.JsonFormat.read", side_effect=IsADirectoryError)
    mocker.patch("builtins.print")

    main.read()
    (args, kwargs) = print.call_args

    assert args[0] == "Invalid path"


@pytest.mark.it("Read function detects invalid format")
def test_main_read_invalid_format(mocker):
    mocker.patch("builtins.input", side_effect=["./test", "not valid"])
    mocker.patch("builtins.print")

    main.read()
    (args, kwargs) = print.call_args

    assert args[0] == "Invalid format"


@pytest.mark.it("Read function detects RuntimeError")
def test_main_read_invalid_runtime(mocker):
    mocker.patch("builtins.input", side_effect=["./test", "json"])
    mocker.patch("formats.json_format.JsonFormat.read", side_effect=RuntimeError)
    mocker.patch("builtins.print")

    main.read()
    (args, kwargs) = print.call_args

    assert args[0] == "Failed to read file"


@pytest.mark.it("Write function correctly writes to path")
def test_main_write_valid(mocker):
    test_data = "test data"

    mocker.patch("builtins.input", side_effect=["./test", "json"])
    mocker.patch("formats.json_format.JsonFormat.write")

    main.write(test_data)
    (args, kwargs) = formats.json_format.JsonFormat.write.call_args

    assert args[0] == "./test"
    assert args[1] == test_data


@pytest.mark.it("Write function detects invalid path")
def test_main_write_invalid_path(mocker):
    mocker.patch("builtins.input", side_effect=["", "json"])
    mocker.patch("formats.json_format.JsonFormat.write", side_effect=FileNotFoundError)
    mocker.patch("builtins.print")

    main.write("test")
    (args, kwargs) = print.call_args

    assert args[0] == "Invalid path"


@pytest.mark.it("Write function detects invalid path with directory")
def test_main_write_invalid_path_directory(mocker):
    mocker.patch("builtins.input", side_effect=["./", "json"])
    mocker.patch("formats.json_format.JsonFormat.write", side_effect=IsADirectoryError)
    mocker.patch("builtins.print")

    main.write("test")
    (args, kwargs) = print.call_args

    assert args[0] == "Invalid path"


@pytest.mark.it("Write function detects invalid format")
def test_main_write_invalid_format(mocker):
    mocker.patch("builtins.input", side_effect=["./test", "not valid"])
    mocker.patch("builtins.print")

    main.write("test")
    (args, kwargs) = print.call_args

    assert args[0] == "Invalid format"


@pytest.mark.it("Write function detects RuntimeError")
def test_main_write_invalid_runtime(mocker):
    mocker.patch("builtins.input", side_effect=["./test", "json"])
    mocker.patch("formats.json_format.JsonFormat.write", side_effect=RuntimeError)
    mocker.patch("builtins.print")

    main.write("test")
    (args, kwargs) = print.call_args

    assert args[0] == "Failed to write data"


@pytest.mark.it("Write function detects if there is no data")
def test_main_write_no_data(mocker):
    mocker.patch("builtins.print")

    main.write(None)
    (args, kwargs) = print.call_args

    assert args[0] == "There is no data to write"


@pytest.mark.it("Display function correctly prints to terminal")
def test_main_display_valid(mocker):
    test_data = "test data"

    mocker.patch("builtins.input", return_value="json")
    mocker.patch("formats.json_format.JsonFormat.display")

    main.display(test_data)
    (args, kwargs) = formats.json_format.JsonFormat.display.call_args

    assert args[0] == "test data"


@pytest.mark.it("Display function detects invalid format")
def test_main_display_invalid_format(mocker):
    mocker.patch("builtins.input", return_value="not valid")
    mocker.patch("builtins.print")

    main.display("test")
    (args, kwargs) = print.call_args

    assert args[0] == "Invalid format"


@pytest.mark.it("Display function detects RuntimeError")
def test_main_display_invalid_runtime(mocker):
    mocker.patch("builtins.input", return_value="json")
    mocker.patch("formats.json_format.JsonFormat.display", side_effect=RuntimeError)
    mocker.patch("builtins.print")

    main.display("test")
    (args, kwargs) = print.call_args

    assert args[0] == "Failed to display data"


@pytest.mark.it("Display function detects if there is no data")
def test_main_display_no_data(mocker):
    mocker.patch("builtins.print")

    main.display(None)
    (args, kwargs) = print.call_args

    assert args[0] == "There is no data to display"
