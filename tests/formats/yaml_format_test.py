import pytest
import yaml
from formats.yaml_format import YamlFormat

test_data = [
    {
        "name": "John",
        "address": "Somewhere 12",
        "phone": "00012345"
    },
    {
        "name": "Alice",
        "address": "Somewhere Else 13",
        "phone": "00023456"
    },
    {
        "name": "Bob",
        "address": "Again Somewhere New 14",
        "phone": "00034567"
    }
]


@pytest.mark.it("Correctly reads valid yaml file")
def test_read_valid():

    data = YamlFormat.read("./tests/test_files/yaml_file.yaml")

    assert data == test_data


@pytest.mark.it("Throws a FileNotFoundError when reading non-existent file")
def test_read_invalid():
    with pytest.raises(FileNotFoundError):
        YamlFormat.read("./DoesNotExist.yaml")


@pytest.mark.it("Throws an IsADirectoryError when given a directory")
def test_read_invalid_is_directory():
    with pytest.raises(IsADirectoryError):
        YamlFormat.read("./")


@pytest.mark.it("Correctly calls yaml.dump with data passed in")
def test_write_valid(mocker):
    mocker.patch("builtins.open")
    mocker.patch("yaml.dump")

    YamlFormat.write("./test", test_data)
    (args, kwargs) = yaml.dump.call_args

    assert args[0] == test_data


@pytest.mark.it("Throws FileNotFoundError when writing to empty path")
def test_write_invalid():
    with pytest.raises(FileNotFoundError):
        YamlFormat.write("", test_data)


@pytest.mark.it("Throws IsADirectoryError when writing to directory")
def test_write_invalid_is_directory():
    with pytest.raises(IsADirectoryError):
        YamlFormat.write("./", test_data)


@pytest.mark.it("Correctly calls yaml.dump with data passed in")
def test_display_valid(mocker):
    mocker.patch("yaml.dump")

    YamlFormat.display(test_data)
    (args, kwargs) = yaml.dump.call_args

    assert args[0] == test_data
