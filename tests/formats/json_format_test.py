import pytest
import json
from formats.json_format import JsonFormat

test_data = [
    {
        "name": "John",
        "address": "Somewhere 12",
        "phone": "00012345"
    },
    {
        "name": "Alice",
        "address": "Somewhere Else 13",
        "phone": "00023456"
    },
    {
        "name": "Bob",
        "address": "Again Somewhere New 14",
        "phone": "00034567"
    }
]


@pytest.mark.it("Correctly reads valid json file")
def test_read_valid():

    data = JsonFormat.read("./tests/test_files/json_file.json")

    assert data == test_data


@pytest.mark.it("Throws a FileNotFoundError when reading non-existent file")
def test_read_invalid():
    with pytest.raises(FileNotFoundError):
        JsonFormat.read("./DoesNotExist.json")


@pytest.mark.it("Throws an IsADirectoryError when given a directory")
def test_read_invalid_is_directory():
    with pytest.raises(IsADirectoryError):
        JsonFormat.read("./")


@pytest.mark.it("Throws a RuntimeError when trying to read a file of incorrect format")
def test_read_invalid_format():
    with pytest.raises(RuntimeError):
        JsonFormat.read("tests/test_files/yaml_file.yaml")


@pytest.mark.it("Correctly calls json.dump with data passed in")
def test_write_valid(mocker):
    mocker.patch("builtins.open")
    mocker.patch("json.dump")

    JsonFormat.write("./test", test_data)
    (args, kwargs) = json.dump.call_args

    assert args[0] == test_data


@pytest.mark.it("Throws FileNotFoundError when writing to empty path")
def test_write_invalid():
    with pytest.raises(FileNotFoundError):
        JsonFormat.write("", test_data)


@pytest.mark.it("Throws IsADirectoryError when writing to directory")
def test_write_invalid_is_directory():
    with pytest.raises(IsADirectoryError):
        JsonFormat.write("./", test_data)


@pytest.mark.it("Correctly calls json.dumps with data passed in")
def test_display_valid(mocker):
    mocker.patch("json.dumps")

    JsonFormat.display(test_data)
    (args, kwargs) = json.dumps.call_args

    assert args[0] == test_data
